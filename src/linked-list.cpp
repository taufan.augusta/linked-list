#include <iostream>
#include <node.h>

Node* head = nullptr;
Node* tail = nullptr;

Node* construct_node(int value)
{
    Node* node = new Node;
    node->value = value;
    node->prev = nullptr;
    node->next = nullptr;
    return node;
}

void node_linking(Node* node, Position position)
{
    if(head == nullptr)
    {
        head = node;
        tail = node;
    }
    else if(position == front)
    {
        head->prev = node;
        node->next = head;
        head = node;
    }
    else if(position == back)
    {   
        tail->next = node;
        node->prev = tail;
        tail = node;
    }
}

void node_listing()
{
    Node *active = head;
    while(active != nullptr)
    {
        std::cout << active->value << " -> ";
        active = active->next;
    }
}
//     help
// [5]-[2]-[6] - before
// [2]-[5]-[6] - after
void front_to_mid_side(Node *helper)
{
    head->next = helper->next;          // [5] ->next = [6]
    head->next->prev = head;            // [6] ->prev = [5]
    helper->next = head;                // [2] ->next = [5]
    head->prev = helper;                // [5] ->prev = [2]
    helper->prev = nullptr;             // [2] ->prev = nullptr
    head = helper;                      // [2] is HEAD

}

//         help
// [5]-[4]-[2]-[6] - before
// [2]-[4]-[5]-[6] - after
void front_to_mid_cross(Node *helper)
{
    helper->next->prev = head;          // [6] ->prev = [5]
    head->next = helper->next;          // [5] ->next = [6]
    head->prev = helper->prev;          // [5] ->prev = [4]
    helper->next = helper->prev;        // [2] ->next = [4]
    helper->next->prev = helper;        // [4] ->prev = [2]
    helper->next->next = head;          // [4] ->next = [2]
    head = helper;                      // [2] is HEAD
    head->prev = nullptr;               // [2] ->prev = nullptr
}

//         help
// [5]-[4]-[2] - before
// [2]-[4]-[5] - after
void front_to_end(Node *helper)
{
    // helper is TAIL
    tail->next = head->next;            // [2] ->next = [4]
    head->next->prev = tail;            // [4] ->prev = [2]
    tail->prev->next = head;            // [4] ->next = [5]
    head->prev = tail->prev;            // [5] ->prev = [4]
    tail = head;                        // new TAIL is old HEAD
    head = helper;                      // new HEAD is old TAIL
    head->prev = nullptr;               // [5] ->next = nullptr
    tail->next = nullptr;               // [2] ->prev == nullptr
}

//    help1help2
// [6]-[8]-[7]-[9] - before
// [6]-[7]-[8]-[9] - after
void mid_to_mid_side(Node *helper1, Node *helper2)
{
    helper2->next->prev = helper1;      // [9] ->prev = [8]
    helper1->next = helper2->next;      // [8] ->next = [9]
    helper2->prev = helper1->prev;      // [7] ->prev = [6]
    helper1->prev->next = helper2;      // [6] ->next = [7]
    helper2->next = helper1;            // [7] ->next = [8]
    helper1->prev = helper2;            // [8] ->prev = [7]
}

//     help1   help2
// [4]-[8]-[6]-[5]-[9] - before
// [4]-[5]-[6]-[8]-[9] - after
void mid_to_mid_cross(Node *helper1, Node *helper2)
{
    helper2->next->prev = helper1;      // [9] ->prev = [8]
    helper1->next = helper2->next;      // [8] ->next = [9]
    helper1->prev->next = helper2;      // [4] ->next = [5]
    helper2->prev = helper1->prev;      // [5] ->prev = [4]
    helper1->prev = helper2->prev;      // [8] ->prev = [6]
    helper2->prev->next = helper2;      // [6] ->next = [8]
    helper1->prev->prev = helper2;      // [6] ->prev = [5]
    helper2->next = helper1->prev;      // [5] ->next = [6]
}

//     help
// [5]-[9]-[7] - before
// [5]-[7]-[9] - after
void end_to_mid_side(Node *helper)
{
    tail->prev = helper->prev;          // [7] ->prev = [5]
    tail->prev->next = tail;            // [5] ->next = [7]
    tail->next = helper;                // [7] ->next = [9]
    helper->prev = tail;                // [9] ->prev = [7]
    tail = helper;                      // [9] is TAIL
    tail->next = nullptr;               // [9] ->next = nullptr
}

//     help
// [4]-[9]-[7]-[5] - before
// [4]-[5]-[7]-[9] - after
void end_to_mid_cross(Node *helper)
{
    tail->prev = helper->prev;          // [5] ->prev = [4]
    tail->prev->next = helper;          // [4] ->next = [5]
    tail->next = helper->next;          // [5] ->next = [7]
    tail->next->next = helper;          // [7] ->next = [9]
    tail->next->next->prev = tail->next; // [9] ->prev = [7]
    tail = helper;                      // [9] is TAIL
    tail->next = nullptr;               // [9] ->next = nullptr
}

// Node* finder()
// {

// }