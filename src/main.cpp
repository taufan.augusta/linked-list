#include <node.h>

int main() 
{
    Node *node_A, *node_B, *node_C, *node_D;
    node_A = construct_node(5);
    node_B = construct_node(10);
    node_C = construct_node(8);
    node_D = construct_node(12);

    node_linking(node_A, back);
    node_linking(node_B, back);
    node_linking(node_C, back);
    node_linking(node_D, back);

    node_listing();

}