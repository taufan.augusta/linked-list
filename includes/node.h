#ifndef NODE
#define NODE

enum Position 
{ 
    front, 
    back 
};

struct Node
{
    int value;
    Node* prev;
    Node* next;
};

Node* construct_node(int value);
void node_linking(Node* node, Position position);
void node_listing();

#endif // !NODE